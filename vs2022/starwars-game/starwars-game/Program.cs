﻿/*------------------------ INTRO --------------------------*/
/*
Console.ForegroundColor = ConsoleColor.Magenta;

var monTitre = "a star wars game".ToUpper();

string format = "{0} ({1})";

Console.WriteLine($"{monTitre.ToUpper()} ({DateTime.Now.ToString("dd/MM/yyyy")})");

monTitre = string.Format(format, monTitre, DateTime.Now);

Console.WriteLine(monTitre);

*/
/*-------------------- EXO 001 + correction -------------------------*/

/*
var monTitre = "a star wars game".ToUpper();
var monSousTitre = "a copycat of Zelda".ToLower();

Console.WriteLine(monTitre);
Console.ForegroundColor = ConsoleColor.Yellow;
Console.WriteLine(monSousTitre);
Console.ForegroundColor = ConsoleColor.Gray;

Console.WriteLine("Quel est votre nom ?");
var nom = Console.ReadLine();
bool dateValide = false;

do
{
    Console.WriteLine("Ta date de naissance stp ?");
    var dateDeNaissance = Console.ReadLine();

    if (DateTime.TryParse(dateDeNaissance, out var maVraiDate))
    {
        var comparaisonDates = DateTime.Now - maVraiDate;
        int nbAnnees = (int)(comparaisonDates.TotalDays / 365);
        Console.WriteLine($"Ton prénom est bien ? {nom}, et tu es âgé de {nbAnnees} années ?");

        dateValide = true;
    }
    else
    {
        Console.ForegroundColor = ConsoleColor.DarkRed;
        Console.WriteLine("Erreur de saisie de date, ré-essaie stp !");
        Console.ForegroundColor = ConsoleColor.White;
    }
}
while (!dateValide);

*/

/*-------------------------- EXO EXEMPLE ------------------------------*/
/*
void Calculer(int val1, int val2, out int result)
{
    {
        int test = 0; //durée de vie des accolades
    }

    result = val1 + val2;
}

// int resultat = 22;
int un = 1;
int deux = 2;
Calculer(un, deux, out var resultat);
Console.WriteLine(resultat);

*/

/*----------------- EXO 002 ----------------------------*/

void afficherMenu() {

    var choix = "0";

    do
    {
        Console.WriteLine("1 - Nouvelle Partie");
        Console.WriteLine("2 - Charger Partie");
        Console.WriteLine("3 - Quitter");

        Console.WriteLine("Que souhaitez vous faire ?");
        choix = Console.ReadLine();

        if (choix == "1")
        {
            Console.WriteLine("Quel est votre nom ?");
            var nom = Console.ReadLine();

            var dateValide = false;
            var ageValide = false;

            do
            {
                Console.WriteLine("Quel est votre date de naissance ?");
                var dateDeNaissance = Console.ReadLine();
                var age = Console.ReadLine();
            

                if (DateTime.TryParse(dateDeNaissance, out var maVraiDate))
                {
                    var comparaisonDates = DateTime.Now - maVraiDate;
                    int nbAnnees = (int)(comparaisonDates.TotalDays / 365);
                    dateValide = true;
                    int intAge = int.Parse(age);

                    if (intAge > 13 && intAge == nbAnnees)
                    {

                    }
                    Console.WriteLine($"Ton prénom est bien ? {nom} et tu as bien {nbAnnees} ans ?");
                }
                else
                {
                    if (dateValide = false) 
                    { 
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("Une erreur sur la date ou le format est survenue, réessayez.");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    if (ageValide = false)
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("Tu es trop jeune pour jouer, réessaie dans quelques années.");
                        Console.ForegroundColor = ConsoleColor.White;
                    }

                }
            }
            while (!dateValide && !ageValide);
        }
    
    }
    while (choix != "3");

}

afficherMenu();